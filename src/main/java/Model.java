package main.java;

import java.util.Random;

public class Model {

    Random rand = new Random();
    private int value = rand.nextInt(100);

    public void setValue(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}
