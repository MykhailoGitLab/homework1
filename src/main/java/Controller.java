package main.java;

import java.util.Scanner;

public class Controller {

    Scanner scanner = new Scanner(System.in);

    private Model model;
    private View view;

    public Controller(Model model, View view) {
        this.model = model;
        this.view = view;
    }

    public void theGame() {
        int yourNumber = scanner.nextInt();
        if (yourNumber > model.getValue()) {
            view.yourNumberIsBigger();
            theGame();
        } else if (yourNumber < model.getValue()) {
            view.yourNumberIsLess();
            theGame();
        } else {
            view.rightNumber();
        }
    }
}


